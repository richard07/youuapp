import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:lottie/lottie.dart';
import 'package:prueba_1/src/pages/login/login_controller.dart';

import 'package:prueba_1/src/utils/my_colors.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final LoginController _con = LoginController();

  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance!.addPersistentFrameCallback((timeStamp) {
      _con.init(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Stack(
          children: [
            Positioned(top: -80, left: -100, child: _circleLogin()),
            Positioned(
              child: _textLogin(),
              top: 60,
              left: 25,
            ),
            SingleChildScrollView(
              child: Column(
                children: [
                  // _imageBanner(),
                  _lottieAnimation(),
                  _texFielEmail(),
                  _texFielPassworld(),
                  _buttonLogin(),
                  _textDonHaveAccount(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _lottieAnimation() {
    return Container(
      margin: EdgeInsets.only(
        top: 160,
        bottom: MediaQuery.of(context).size.height * .10,
      ),
      child: Lottie.asset('assets/json/youu.json',
          width: 380, height: 250, fit: BoxFit.fill),
    );
  }

  Widget _circleLogin() {
    return Container(
      width: 290,
      height: 230,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          color: MyColors.primaryColors),
    );
  }

  Widget _textDonHaveAccount() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          '¿No tienes cuenta?',
          style: TextStyle(color: MyColors.primaryColors),
        ),
        const SizedBox(width: 7),
        GestureDetector(
          onTap: _con.gotoPageNavigator,
          child: Text('Registrate',
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: MyColors.primaryColors)),
        )
      ],
    );
  }

  Widget _textLogin() {
    return const Text(
      'LOGIN',
      style: TextStyle(
          color: Colors.white, fontWeight: FontWeight.bold, fontSize: 22),
    );
  }

  Widget _buttonLogin() {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 50, vertical: 30),
      child: ElevatedButton(
        onPressed: _con.login,
        child: const Text('Ingresar'),
        style: ElevatedButton.styleFrom(
            primary: MyColors.primaryColors,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
            padding: EdgeInsets.symmetric(vertical: 15)),
      ),
    );
  }

  Widget _texFielPassworld() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
      decoration: BoxDecoration(
          color: MyColors.primaryOpacityColor,
          borderRadius: BorderRadius.circular(30)),
      child: TextField(
        controller: _con.passwordController,
        obscureText: true,
        decoration: InputDecoration(
            hintText: 'Contraseña',
            border: InputBorder.none,
            contentPadding: EdgeInsets.all(15),
            hintStyle: TextStyle(color: MyColors.primaryColorsDark),
            prefixIcon: Icon(Icons.lock, color: MyColors.primaryColors)),
      ),
    );
  }

  Widget _texFielEmail() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
      decoration: BoxDecoration(
          color: MyColors.primaryOpacityColor,
          borderRadius: BorderRadius.circular(30)),
      child: TextField(
        controller: _con.emailController,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
            hintText: 'Correo Electronico',
            border: InputBorder.none,
            contentPadding: EdgeInsets.all(15),
            hintStyle: TextStyle(color: MyColors.primaryColorsDark),
            prefixIcon: Icon(Icons.email, color: MyColors.primaryColors)),
      ),
    );
  }

//  Widget _imageBanner() {
//    return Container(
//      margin: EdgeInsets.only(
//        top: 100,
//        bottom: MediaQuery.of(context).size.height * .22,
//      ),
//      child: Image.asset(
//        'assets/img/delivery.png',
//        width: 200,
//        height: 200,
//      ),
//    );
//  }
}

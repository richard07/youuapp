import 'package:flutter/material.dart';
import 'package:prueba_1/src/models/response_api.dart';
import 'package:prueba_1/src/models/user.dart';
import 'package:prueba_1/src/utils/my_snackbar.dart';

import '../../provider/user_provider.dart';

class RegisterController {
  BuildContext? context;
  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController lastnameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirpasswordController = TextEditingController();

  UserProvider userProvider = UserProvider();

  Future? init(BuildContext context) {
    context = context;
    userProvider.init(context);
    return null;
  }

  void register() async {
    String email = emailController.text.trim();
    String name = nameController.text;
    String lastname = lastnameController.text;
    String phone = phoneController.text.trim();
    String password = passwordController.text.trim();
    String confirmpassword = confirpasswordController.text.trim();

    if (email.isEmpty ||
        name.isEmpty ||
        lastname.isEmpty ||
        phone.isEmpty ||
        password.isEmpty ||
        confirmpassword.isEmpty) {
      Mysnackbar.show(context, 'Debes ingresar todos los campos');

      return;
    }

    if (confirmpassword != password) {
      Mysnackbar.show(context, 'Las contraseña no coiciden');
      return;
    }

    if (password.length < 6) {
      Mysnackbar.show(context, 'Las contraseña debe tener almenos 6 carateres');
      return;
    }

    User user = User(
        email: email,
        name: name,
        lastname: lastname,
        phone: phone,
        password: password);

    ResponseApi? responseApi = await userProvider.create(user);
    if (responseApi != null) {
      print('RESPUESTA: ${responseApi.toJson()}');
    } else {
      print('LA RESPUESTA FUE NULA');
    }

    Mysnackbar.show(context, responseApi?.message!);
  }
}

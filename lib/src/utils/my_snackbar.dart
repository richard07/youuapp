import 'package:flutter/material.dart';

class Mysnackbar {
  static String? get text => null;

  static void show(BuildContext? context, String) {
    if (context == null) {
      return;
    }

    FocusScope.of(context).requestFocus(FocusNode());

    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
        text!,
        textAlign: TextAlign.center,
        style: const TextStyle(color: Colors.white, fontSize: 14),
      ),
      backgroundColor: Colors.black,
      duration: const Duration(seconds: 3),
    ));
  }
}

import 'package:flutter/cupertino.dart';

class MyColors {
  static Color primaryColors = const Color(0xFF24B014);
  static Color primaryColorsDark = const Color.fromARGB(255, 21, 102, 12);
  static Color primaryOpacityColor = Color.fromARGB(255, 221, 250, 219);
}

// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  String? id;
  String? name;
  String? lastname;
  String? email;
  String? phone;
  String? password;
  String? confirmpassword;
  String? session_token;
  String? image;

  User({
    this.id,
    this.name,
    this.lastname,
    this.email,
    this.phone,
    this.password,
    this.confirmpassword,
    this.session_token,
    this.image,
  });

  factory User.fromJson(Map<dynamic, dynamic> json) => User(
        id: json["id"],
        name: json["name"],
        lastname: json["lastname"],
        email: json["email"],
        phone: json["phone"],
        password: json["password"],
        confirmpassword: json["confirmpassword"],
        session_token: json["session_token"],
        image: json["image"],
      );

  Map<dynamic, dynamic> toJson() => {
        "id": id,
        "name": name,
        "lastname": lastname,
        "email": email,
        "phone": phone,
        "password": password,
        "confirmpassword": confirmpassword,
        "session_token": session_token,
        "image": image,
      };
}

import 'package:flutter/material.dart';
import 'package:prueba_1/src/pages/login/login_page.dart';
import 'package:prueba_1/src/pages/register/register_page.dart';

import 'package:prueba_1/src/utils/my_colors.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'App prueba',
      initialRoute: 'login',
      routes: {
        'login': (BuildContext contex) => LoginPage(),
        'register': (BuildContext contex) => RegisterPage(),
      },
      theme: ThemeData(
        fontFamily: 'Roboto',
        primaryColor: MyColors.primaryColors,
      ),
    );
  }
}
